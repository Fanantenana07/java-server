import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerFile extends  Thread{
    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(9696);
            System.out.println("Le serveur est démarré et en attente de connexion...");

            // Attente de connexion d'un client
            Socket clientSocket = serverSocket.accept();
            System.out.println("Un client est connecté.");

            // Réception du fichier XML
            InputStream inputStream = clientSocket.getInputStream();
            byte[] xmlBytes = inputStream.readAllBytes();

            // Traitement du fichier XML
            String xmlString = new String(xmlBytes);
            // Effectue ici le traitement du fichier XML selon tes besoins
            System.out.println("Fichier XML reçu du client :\n" + xmlString);

            // Envoi d'une réponse au client
            OutputStream outputStream = clientSocket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream, true);
            printWriter.println("Fichier XML reçu avec succès !");

            // Fermeture des flux et de la connexion
            printWriter.close();
            outputStream.close();
            inputStream.close();
            clientSocket.close();
            serverSocket.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new ServerFile().start();
    }
}
