import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.io.File;

public class ReadXML {
    public static void main(String[] args) {
        try {
            // Chargement du fichier XML
            File file = new File("utilisateur.xml");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(file);

            // Récupération de la racine du document
            Node racine = doc.getDocumentElement();

            // Parcours des éléments fils de la racine
            NodeList elements = racine.getChildNodes();
            for (int i = 0; i < elements.getLength(); i++) {
                Node element = elements.item(i);
                if (element.getNodeType() == Node.ELEMENT_NODE) {
                    String nomElement = element.getNodeName();
                    String valeurElement = element.getTextContent();
                    System.out.println(nomElement + " : " + valeurElement);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}