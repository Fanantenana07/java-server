import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.io.File;
import java.util.Scanner;

public class CreateXml {
    public static void main(String[] args) {
        try {
            // Création du document XML
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            // Saisie des données en console
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nom : ");
            String nom = scanner.nextLine();
            System.out.print("Prénom : ");
            String prenom = scanner.nextLine();
            System.out.print("Date : ");
            String date = scanner.nextLine();

            // Création de l'élément racine
            Element racine = doc.createElement("Utilisateur");
            doc.appendChild(racine);

            // Ajout des éléments pour nom, prénom et date
            Element nomElement = doc.createElement("Nom");
            nomElement.appendChild(doc.createTextNode(nom));
            racine.appendChild(nomElement);

            Element prenomElement = doc.createElement("Prénom");
            prenomElement.appendChild(doc.createTextNode(prenom));
            racine.appendChild(prenomElement);

            Element dateElement = doc.createElement("Date");
            dateElement.appendChild(doc.createTextNode(date));
            racine.appendChild(dateElement);

            // Génération du fichier XML
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.transform(new DOMSource(doc), new StreamResult(new File("utilisateur.xml")));

            System.out.println("Le fichier XML a été créé avec succès !");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
