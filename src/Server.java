import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;


public class Server extends  Thread {
    private int idClient=0;
    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(9696);
            Connection connection = DBClass.getConnection();
            while (true){
                System.out.println("attente d'un client");
                Socket socket = serverSocket.accept();
                idClient++;
                System.out.println("client connecter "+ idClient);
                new ThreadSocket(socket,connection,idClient).start();
            }

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new Server().start();
    }
}
