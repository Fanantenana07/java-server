import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBClass {
    public static Connection getConnection () throws ClassNotFoundException{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet","root","");
            System.out.println("connexion etablie");
            return con;
        } catch (SQLException e) {
            e.printStackTrace();
            return  null;
        }
    }
}
