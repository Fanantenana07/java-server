import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.google.gson.Gson;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ThreadSocket extends Thread {
    private Socket socket;
    private Connection con;
    private int id;

    public ThreadSocket(Socket socket, Connection con, int id) {
        super();
        this.socket = socket;
        this.id = id;
        this.con = con;
    }

    @Override
    public void run() {
        try (
                InputStream inputStream = socket.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream())
        ) {
            while (!socket.isClosed()) {
                String request = bufferedReader.readLine();
                if (request != null) {
                    System.out.println(request);
                    String action = request.split(" ")[0];

                    if (con != null && !con.isClosed()) {
                        if (action.startsWith("INSERT")) {
                            System.out.println("ENTRER INSERTION");
                            insertEtudiant(request);

                        } else if (action.startsWith("UPDATE")) {
                            System.out.println("ENTRER update");
                            updateEtudiant(request);
                        } else if (action.startsWith("DELETE")) {
                            System.out.println("ENTRER DELETE");
                            deleteEtudiant(request);
                        } else if (action.startsWith("GET")) {
                            System.out.println("ENTRER GET");
                            getEtudiant();
                        }
                    } else {
                        System.out.println("erreur");
                        PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                        printWriter.println("Erreur dans la base de donne! impossible de se connecter!");
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String AllEtudiant() throws IOException {
        try {

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `etudiant`");
            List<Etudiant> etudiants = new ArrayList<>();
            Gson gson = new Gson();
            while (rs.next()) {
                Etudiant et = new Etudiant(rs.getString("matricule"),
                        rs.getString("nom"),
                        rs.getString("bourse"),
                        rs.getString("adresse"));
                etudiants.add(et);
            }
            String jsonString = gson.toJson(etudiants);
            return jsonString;
        } catch (Exception e) {
            // Gérer les exceptions
            return "";
        }
    }

    public void getEtudiant() throws IOException {
        try {
            OutputStream outputStream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream, true);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `etudiant`");
            List<Etudiant> etudiants = new ArrayList<>();
            Gson gson = new Gson();
            while (rs.next()) {
                Etudiant et = new Etudiant(rs.getString("matricule"),
                        rs.getString("nom"),
                        rs.getString("bourse"),
                        rs.getString("adresse"));
                etudiants.add(et);
            }
            String jsonString = gson.toJson(etudiants);
            printWriter.println(jsonString.toString());
        } catch (Exception e) {
            // Gérer les exceptions
        }
    }

    public void insertEtudiant(String request) throws SQLException, IOException {
        try (

                PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                PreparedStatement st = con.prepareStatement(request)
        ) {
            st.execute();
            String etudiantsString = AllEtudiant();
            printWriter.write(etudiantsString.toString());
        }
    }

    public void deleteEtudiant(String request) throws SQLException, IOException {
        try (
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                PreparedStatement st = con.prepareStatement(request)
        ) {
            st.execute();
            String etudiantsString = AllEtudiant();
            printWriter.write(etudiantsString.toString());
        }
    }

    public void updateEtudiant(String request) throws SQLException, IOException {
        try (
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                PreparedStatement st = con.prepareStatement(request)
        ) {
            st.execute();
            String etudiantsString = AllEtudiant();
            printWriter.write(etudiantsString.toString());
        }
    }
}
