import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client extends  Thread {
    @Override
    public void run() {
        try {
            Socket socket = new Socket("localhost",9696);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream, true);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while (true){
                Scanner scanner = new Scanner(System.in);
                System.out.println("Saisir un message");
                String message = scanner.nextLine();
                printWriter.println(message);
                String reponse = bufferedReader.readLine();
                System.out.println(reponse);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static void main(String[] args) throws IOException {

        new Client().start();
    }
}
